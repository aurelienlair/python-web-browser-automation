from pages.authentication_page import AuthenticationPage
from pages.profile_page import ProfilePage
from decouple import config
import allure


@allure.feature("Login")
class TestLogin:
    @allure.title("User logs in with valid credentials")
    def test_log_in_successfully(self):
        authentication_page = AuthenticationPage()
        authentication_page.open()
        authentication_page.login(config("LOGIN"), config("PASSWORD"))
        profile_page = ProfilePage()
        profile_page.contains(F"{config('PROFILE_FIRSTNAME')} {config('PROFILE_LASTNAME')}")

    @allure.title("Can't log in with a wrong password")
    def test_can_not_log_in_with_a_wrong_password(self):
        authentication_page = AuthenticationPage()
        authentication_page.open()
        authentication_page.login(config("LOGIN"), "THIS_IS_INDEED_A_WRONG_PASSWORD")
        authentication_page.check_if_toast_message_appeared("Auth failed")
