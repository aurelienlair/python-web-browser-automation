import pytest
from decouple import config


class TestDefault:
    @pytest.fixture(autouse=True)
    def setup1(self):
        self.value = 3 + 5

    def test_default_addition(self):
        url = config('BASE_URL')
        assert 'https://localcoding.us' in url
        assert self.value == 8
