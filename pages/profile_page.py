from pages.base_page import BasePage
from selenium.webdriver.common.by import By


class ProfilePage(BasePage):
    AVATAR_SQUARE = (By.CSS_SELECTOR, "span.ant-avatar-square")
    HEADER = (By.CSS_SELECTOR, "h1")

    def contains(self, profile_name):
        # Check that the span avatar is present
        self.wd.find_element(*self.AVATAR_SQUARE)
        self.wd.wait_for_text_to_be_present(
            self.HEADER[0],
            self.HEADER[1],
            profile_name
        )
