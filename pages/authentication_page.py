from pages.base_page import BasePage
from selenium.webdriver.common.by import By


class AuthenticationPage(BasePage):
    # Selectors first as PageObject Model pattern in Python
    INPUT_USERNAME = (By.CSS_SELECTOR, "#normal_login_email")
    INPUT_PASSWORD = (By.CSS_SELECTOR, "#normal_login_password")
    BUTTON_SUBMIT = (By.CSS_SELECTOR, "button[type='submit'].login-form-button")
    NOTIFICATION_MESSAGE = (By.CSS_SELECTOR, "div.ant-notification-notice-message")

    # Then methods
    def open(self):
        self.wd.open("/user/login")

    def login(self, username, password):
        input_username = self.wd.find_element(*self.INPUT_USERNAME)
        input_username.send_keys(username)
        input_password = self.wd.find_element(*self.INPUT_PASSWORD)
        input_password.send_keys(password)
        button_submit = self.wd.find_element(*self.BUTTON_SUBMIT)
        button_submit.click()

    def check_if_toast_message_appeared(self, text):
        self.wd.wait_for_text_to_be_present(
            self.NOTIFICATION_MESSAGE[0],
            self.NOTIFICATION_MESSAGE[1],
            text
        )
