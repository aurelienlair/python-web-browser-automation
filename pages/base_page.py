import pytest


class BasePage:
    def __init__(self):
        if hasattr(pytest, "wd"):
            self.wd = pytest.wd
