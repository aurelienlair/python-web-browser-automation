# Web browser test automation

## Description

This project is an experimental learning of a web browser automation testing in [Python](https://en.wikipedia.org/wiki/Python_(programming_language)). It tests the authentication on a [dummy website](https://localcoding.us):
<img src="docs/images/dummy_website.png" alt="login" width="400"/>

It uses [Selenium](https://en.wikipedia.org/wiki/Selenium_(software)) as testing framework and in particular [Selenium WebDriver](https://www.selenium.dev/documentation/webdriver/) to interacts with a web browser ([Google Chrome](https://en.wikipedia.org/wiki/Google_Chrome)).

The outcome of the test suite can be visualized thanks to [Allure](http://allure.qatools.ru/) test automation report.

Testing can be done either locally or throughout the ad-hoc [Gitlab](https://gitlab.com/aurelienlair/python-web-browser-automation/-/pipelines) [CD/CI](https://en.wikipedia.org/wiki/CI/CD) pipeline.

## Getting Started

The following instructions will allow you to set the project up from scratch.

## System requirements

- [PyCharm Community Edition](https://www.jetbrains.com/pycharm/) as IDE (the latest version)
- [PyEnv](https://github.com/pyenv/pyenv) as Python Version Management (we used 3.10.0 Python version)
- [Java 8](https://www.oracle.com/java/technologies/downloads/)
- [Allure](https://docs.qameta.io/allure/)

## Allure report

Before generating the report please ensure that while invoking pytest with PyCharm its report
directory is correctly configured:
<img src="docs/images/pytest_allure.png" alt="login" width="400"/>

Then from the terminal run the below command once the test suite has run to generate the reports

```bash
allure generate allure-results -o allure-report
```

Then open it

```bash
allure open
```

<img src="docs/images/allure_report.png" alt="login" width="600"/>

If needed you can use this command to clean the reports

```bash
allure generate --clean
```

### Report in pipeline

You can access the reports (with the history) generated in pipeline with Gitlab pages by opening [this](https://aurelienlair.gitlab.io/python-web-browser-automation/) url.

## Page Object Pattern

This project is using the [Page object](https://www.selenium.dev/documentation/en/guidelines_and_recommendations/page_object_models/), a design pattern that is recommended as best practices.
The functionality classes (PageObjects) in this design represent a logical relationship between the pages of the application.

The current project takes also inspiration of:

> A page object wraps an HTML page, or fragment, with an application-specific API, allowing you to manipulate page elements without digging around in the HTML.
>
> <cite> [Martin Fowler](http://martinfowler.com/bliki/PageObject.html) </cite>

> By introducing the "*elements as first class citizens*" principle it is now possible to build up large test suites using this pattern.
> There are no additional packages required to create page objects.
> It turns out that `Object.create` provides all necessary features we need:
>
> - Inheritance between page objects
> - Lazy loading of elements
> - Encapsulation of methods and actions
>
> The goal behind page objects is to abstract any page information away from the actual tests.
> Ideally you should store all selectors or specific instructions that are unique for a certain page in a page object, so that you still can run your test after you’ve completely redesigned your page.
>
> <cite> [WebDriverIO](http://webdriver.io/guide/testrunner/pageobjects.html) </cite>

## Git commit message convention

This projects follows the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/).

```shell
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

Example:

```shell
docs: add description of Python run command
```

| Type | Description |
|------| ----------- |
| `style` | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) |
| `build` | Changes to the build process |
| `chore` | Changes to the build process or auxiliary tools and libraries such as documentation generation |
| `docs` | Documentation updates |
| `feat` | New features |
| `fix`  | Bug fixes |
| `refactor` | Code refactoring |
| `test` | Adding missing tests |
| `perf` | A code change that improves performance |

## Useful links

- [Page object model best practise](https://www.selenium.dev/documentation/test_practices/encouraged/page_object_models)
- [Python W3C documentation](https://www.w3schools.com/python)
- [Selenium with Python](https://selenium-python.readthedocs.io/)
- [Code With Me](https://www.jetbrains.com/code-with-me/) is really nice to work in [pair programming](https://en.wikipedia.org/wiki/Pair_programming)
