from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from decouple import config
import time


class WebDriver(webdriver.Remote):
    def __init__(self):
        self.base_url = config("BASE_URL")
        self.timeout = int(config("TIMEOUT"))
        self.window_width = int(config("WINDOW_WIDTH"))
        self.window_height = int(config("WINDOW_HEIGHT"))
        options = webdriver.ChromeOptions()
        options.add_argument(F"window-size={self.window_width},{self.window_height}")
        if config("HEADLESS") == "true":
            options.add_argument("--headless")
            options.add_argument("--disable-gpu")
        webdriver.Chrome.__init__(
            self,
            executable_path=ChromeDriverManager().install(),
            options=options,
            desired_capabilities=webdriver.DesiredCapabilities.CHROME
        )
        self.set_window_size(self.window_width, self.window_height)
        # Setting a global implicit timeout used for all the elements which value is set in config file
        self.implicitly_wait(self.timeout)

    def open(self, path):
        self.get(self.base_url + path)

    def wait_for_text_to_be_present(self, how, what, value):
        for i in range(self.timeout * 2):
            element = self.find_element(how, what)
            if element.text == value:
                return
            else:
                time.sleep(0.5)
        assert False, F"Text '{value}' didn't appear in {self.timeout} seconds"
