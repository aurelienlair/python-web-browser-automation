import pytest
import allure
from modules.webdriver import WebDriver


def pytest_addoption(parser):
    parser.addoption("--timeout", action="store", default=10)


@pytest.fixture(autouse=True)
def project_init(request):
    timeout = int(request.config.getoption("timeout"))


@pytest.fixture(autouse=True)
def wd(request):
    driver = WebDriver()
    pytest.wd = driver
    yield driver
    if request.node.rep_call.failed:
        allure.attach(
            driver.get_screenshot_as_png(),
            name=request.function.__name__,
            attachment_type=allure.attachment_type.PNG
        )
    driver.quit()


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)
    return rep

